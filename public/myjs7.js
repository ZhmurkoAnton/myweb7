$(document).ready(function(){//инициализация jquery
  $('.slider').slick({
    arrows:true,
    dots:true,
    adaptiveHeight:true,
    slidesToShow:3,
    slidesToScroll:3,
    speed:1000,
    infinite:true,
    initialSlide:0,
    autoplay:false,
    touchThreshold:3,
    responsive: [
    {
      breakpoint: 1490,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
      }
    },
    {
      breakpoint: 1040,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
      }
    }
    ]
  });
});